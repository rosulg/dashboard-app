# Sample user management app

Simple user management app built with Angular and Spring Boot.
The application uses JWT to authenticate requests.

**JAVA_VERSION: 1.8**

The Spring Boot part of the application is built on top of https://github.com/murraco/spring-boot-jwt
## Credentials
Default user credentials:
```
username: admin
password: password
role: ROLE_ADMIN
```
```
username: admin1
password: password
role: ROLE_ADMIN
```
```
username: admin2
password: password
role: ROLE_ADMIN
```
```
username: client
password: password
role: ROLE_CLIENT
```
# Instructions
**NB! All instructions are from root**
## Development

#### To run the Angular app:
```
cd client
npm install
npm start
```

**npm start** starts the development server on http://localhost:4200/

### To run the Spring Boot app:

```
cd server
mvn clean install
mvn spring-boot:run
```

**mvn spring-boot:run** starts the server on http://localhost:8080/
**Swagger documentation is then available at http://localhost:8080/swagger-ui.html**

## Production build

Angular part:
```
cd client
npm install ( if not done already )
npm run build:tomcat
```

**npm run build:tomcat** -- this bundles the Angular code and outputs it to server (server/src/main/resources/static)

Spring Boot part:
```
mvn clean install
java -jar server/target/user-management-app-1.0.0.jar
```

This runs the app on http://localhost:8080/



