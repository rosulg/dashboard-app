import {FormGroup} from '@angular/forms';

export function getFormControlError(formGroup: FormGroup, fieldName: string): string {

  const errors = formGroup.get(fieldName);

  if (errors) {
    if (errors.hasError('required')) {
      return 'Please fill this field';
    } else if (errors.hasError('maxlength')) {
      return `The maximum length for this field is: ${errors.errors.maxlength.requiredLength}`;
    } else if (errors.hasError('minlength')) {
      return `The minimum length for this field is: ${errors.errors.minlength.requiredLength}`;
    } else if (errors.hasError('pattern')) {
      return 'Incorrect format';
    } else if (errors.hasError('email')) {
      return 'Incorrect email format';
    }
  }

  return '';
}

export function handleError(self: any, err): void {
  if (err && err.error) {
    self.errorMsg = err.error.message;
} else {
    self.errorMsg = 'Something went wrong';
}
}
