import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {User} from '../../modal/user';
import {handleError} from '../../common/helper';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  @Output() editClicked = new EventEmitter<{user: User}>();

  users = [];
  errorMsg = '';

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getRelatedUsersList().subscribe(res => {
      if (res && res.data) {
        this.users = res.data;
      }
    }, err => handleError(this, err));
  }

  emitUser(user: User): void {
    this.editClicked.emit({user});
  }

}
