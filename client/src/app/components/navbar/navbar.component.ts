import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {TokenService} from '../../services/token.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  collapsed = true;

  constructor(private router: Router, private tokenService: TokenService) { }

  toggleNavBar(): void {
    this.collapsed = !this.collapsed;
  }

  logout(): void {
    this.tokenService.destroyAccessToken();
    this.router.navigate(['/']);
  }

}
