import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {getFormControlError} from '../../common/helper';
import {Router} from '@angular/router';
import {TokenService} from '../../services/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private apiService: ApiService, private tokenService: TokenService, public router: Router) { }

  loginForm: FormGroup;
  errorMsg = '';

  ngOnInit() {
    // Destroy token to avoid problems with JWT interceptor
    this.tokenService.destroyAccessToken();
    this.loginForm = this.createFormGroup();
  }

  login(e): void {
    e.preventDefault();
    this.errorMsg = '';
    this.apiService.login(this.loginForm.getRawValue()).subscribe(res => {
      if (res && res.data) {

        this.tokenService.setAccessToken(res.data.token);
        this.router.navigate(['/dashboard']);
      }
    }, err => {
      if (err && err.error) {
        this.errorMsg = err.error.message;
      } else {
        this.errorMsg = 'Something went wrong';
      }
    });
  }

  getFormControlError(fieldName: string): string {
    return getFormControlError(this.loginForm, fieldName);
  }

  private createFormGroup(): FormGroup {
    const controls = {
      username: new FormControl(null, [
        Validators.required, Validators.minLength(4),
        Validators.maxLength(255)
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(8)
      ]),
    };
    return new FormGroup(controls);
  }

}
