import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {getFormControlError, handleError} from '../../common/helper';
import {ApiService} from '../../services/api.service';
import {User} from '../../modal/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  constructor(private apiService: ApiService, private router: Router) { }

  @Input() user: User;
  @Input() action = 'add';

  @Output() formButtonClicked = new EventEmitter<boolean>();

  userForm: FormGroup;
  errorMsg = '';
  ready = false;
  countries: [{name: string, code: string}];

  ngOnInit() {

    if (!this.user || this.action === 'add') {
      this.user = {
        firstName: '',
        lastName: '',
        username: '',
        password: '',
        countryCode: '',
        address: '',
      };
    }
    this.userForm = this.createFormGroup();
    this.apiService.getCountryList().subscribe(res => {
      if (res && res.data) {
        this.countries = res.data;
        // Set flag
        this.ready = true;
      }
    }, err => handleError(this, err));
  }

  getFormControlError(fieldName: string): string {
    return getFormControlError(this.userForm, fieldName);
  }

  private createUser(): void {
    this.apiService.createUser(this.userForm.getRawValue()).subscribe(res => this.handleFormSaveSuccess(res),
        err => handleError(this, err));
  }

  private editUser(): void {
    // Add user id to the form object
    this.apiService.updateUser(Object.assign(this.userForm.getRawValue(), {id: this.user.id})).subscribe(
      res => this.handleFormSaveSuccess(res), err => handleError(this, err));
  }

  private handleFormSaveSuccess(res) {
    if (res && res.data) {
      this.formButtonClicked.emit(true);
    }
  }

  submitForm(e) {
    e.preventDefault();
    if (this.userForm.valid) {
      if (this.action === 'edit') {
        this.editUser();
      } else {
        this.createUser();
      }
    } else {
      alert('Please resolve errors');
    }
  }

  private createFormGroup(): FormGroup {
    // Deep clone
    const user = JSON.parse(JSON.stringify(this.user));
    const controls = {
      firstName: new FormControl(user.firstName, [Validators.required, Validators.maxLength(255)]),
      lastName: new FormControl(user.lastName, [Validators.required, Validators.maxLength(255)] ),
      countryCode: new FormControl(user.countryCode, [Validators.pattern('[A-Z]{2,2}')]),
      address: new FormControl(user.address, Validators.maxLength(255))
    };
    if (this.action === 'add') {
      Object.assign(controls, {
        username: new FormControl(user.username, [Validators.required,
          Validators.minLength(4), Validators.maxLength(255)]),
        password: new FormControl(user.password, [Validators.required,
          Validators.minLength(8), Validators.maxLength(255)]),
      });
    }
    return new FormGroup(controls);
  }

}
