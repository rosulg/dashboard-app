import { Component, OnInit } from '@angular/core';
import {User} from '../../modal/user';
import {ApiService} from '../../services/api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  readonly userListView: number = 0;
  readonly userView: number = 1;
  userFormAction = 'add';
  user: User;

  activeView = 0;
  isAdmin = false;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.activeView = this.userListView;
    this.apiService.me().subscribe(res => {
      if (res && res.data) {
        this.isAdmin = res.data.roles.includes('ROLE_ADMIN');
      }
    });
}

  initAddUserView() {
    this.userFormAction = 'add';
    this.activeView = this.userView;
  }

  initEditUserView(user: User) {
    this.user = user;
    this.activeView = this.userView;
    this.userFormAction = 'edit';
  }

  onEditClicked(event) {
    this.initEditUserView(event.user);
  }

  onClientFormButtonClicked(event) {
    if (event) {
      this.activeView = this.userListView;
    }
  }

  cancel() {
    this.activeView = this.userListView;
  }

}
