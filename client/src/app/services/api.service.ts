import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginData} from '../modal/loginData';
import {ApiResponse} from '../modal/apiresponse';
import {User} from '../modal/user';

@Injectable({
  providedIn: 'root'
})
// This api service is secure only via https
export class ApiService {

  // Should be in a config
  private apiUrl = 'http://localhost:8080';

  constructor(private httpClient: HttpClient) {}

  login(payload: LoginData): Observable<ApiResponse> {
    return this.httpClient.post<ApiResponse>(`${this.apiUrl}/users/login`, payload, this.createHeaders());
  }

  getRelatedUsersList(): Observable<ApiResponse> {
    return this.httpClient.get<ApiResponse>(`${this.apiUrl}/users/related_users`, this.createHeaders());
  }

  createUser(payload: User): Observable<ApiResponse> {
    return this.httpClient.post<ApiResponse>(`${this.apiUrl}/users/create`, payload, this.createHeaders());
  }

  updateUser(payload: User): Observable<ApiResponse> {
    return this.httpClient.put<ApiResponse>(`${this.apiUrl}/users/update`, payload, this.createHeaders());
  }

  getCountryList(): Observable<ApiResponse> {
    return this.httpClient.get<ApiResponse>(`${this.apiUrl}/countries`, this.createHeaders());
  }

  me(): Observable<ApiResponse> {
    return this.httpClient.get<ApiResponse>(`${this.apiUrl}/users/me`, this.createHeaders());
  }

  refreshToken(): Observable<ApiResponse> {
    return this.httpClient.get<ApiResponse>(`${this.apiUrl}/users/refresh`, this.createHeaders());
  }

  private createHeaders(): object {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
    };
    return {headers: new HttpHeaders(headers), withCredentials: true};
  }

}
