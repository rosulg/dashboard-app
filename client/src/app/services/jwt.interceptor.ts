import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {TokenService} from './token.service';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(public tokenService: TokenService, private router: Router) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.tokenService.hasAccessToken()) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.tokenService.getAccessToken()}`
        }
      });
    }

    return next.handle(request).pipe(
      tap(
        event => this.handleResponse(request, event),
        error => this.handleError(request, error)
      )
    );
  }

  handleResponse(req: HttpRequest<any>, event): void {}

  handleError(req: HttpRequest<any>, event): void {
    if (event.status === 401) {
      this.tokenService.destroyAccessToken()
      this.router.navigate([ '/' ]);
    }
  }
}
