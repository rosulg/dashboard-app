
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import {TokenService} from './token.service';

@Injectable({
  providedIn: 'root'
})
export class RouteGuardianService implements CanActivate {

  constructor(private tokenService: TokenService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (!this.tokenService.hasAccessToken()) {
      this.router.navigate([ '/' ]);
    }
    return this.tokenService.hasAccessToken();
  }
}
