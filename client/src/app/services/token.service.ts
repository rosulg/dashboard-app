import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  getAccessToken(): string {
    return localStorage.getItem('token');
  }

  setAccessToken(token: string) {
    localStorage.setItem('token', token);
  }

  hasAccessToken(): boolean {
    return !!localStorage.getItem('token');
  }

  destroyAccessToken(): void {
    localStorage.removeItem('token');
  }
}
