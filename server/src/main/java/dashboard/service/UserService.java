package dashboard.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import dashboard.exception.CustomException;
import dashboard.model.User;
import dashboard.repository.UserRepository;
import dashboard.security.JwtTokenProvider;

import java.util.List;

@Service
public class UserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private JwtTokenProvider jwtTokenProvider;

  @Autowired
  private AuthenticationManager authenticationManager;

  public String login(String username, String password) {
    try {
      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
      return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).getRoles());
    } catch (AuthenticationException e) {
      throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

  public String create(User user) {
    if (!userRepository.existsByUsername(user.getUsername())) {
      user.setPassword(passwordEncoder.encode(user.getPassword()));
      userRepository.save(user);
      return jwtTokenProvider.createToken(user.getUsername(), user.getRoles());
    } else {
      throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

  public User updateUser(User user) {
    User toBeUpdatedUser = userRepository.findOne(user.getId());
    if (toBeUpdatedUser != null) {
      // Set updatable fields
      toBeUpdatedUser.setFirstName(user.getFirstName());
      toBeUpdatedUser.setLastName(user.getLastName());
      toBeUpdatedUser.setAddress(user.getAddress());
      toBeUpdatedUser.setCountryCode(user.getCountryCode());
      return userRepository.save(toBeUpdatedUser);
    }
    return null;
  }

  public void delete(String username) {
    userRepository.deleteByUsername(username);
  }

  public User search(String username) {
    User user = userRepository.findByUsername(username);
    if (user == null) {
      throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
    }
    return user;
  }

  public List<User> findUsersByUser(User user) {
    if (user != null) {
      return userRepository.findUsersByUser(user.getId());
    }
    return null;
  }

  public List<User> findAllUsers() {
    return userRepository.findAll();
  }

  public User whoAmI(HttpServletRequest req) {
    String token = jwtTokenProvider.resolveToken(req);
    if (token != null) {
      return userRepository.findByUsername(jwtTokenProvider.getUsername(token));
    }
    return null;
  }

  public String refreshToken(String username) {
    return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).getRoles());
  }

}
