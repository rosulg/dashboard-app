package dashboard.service;

import dashboard.model.Country;
import dashboard.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryService {

  @Autowired
  private CountryRepository countryRepository;

  public List<Country> getCountryList() {
    return countryRepository.findAll();
  }
}
