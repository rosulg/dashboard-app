package dashboard.controller;

import io.swagger.annotations.Api;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Api(tags = "error")
/**
 * The sole reason of this controller is to redirect failed request to the index.html.
 * That way the user always sees the Angular application.
 * Remove this if you are not hosting the Angular app from the same embedded Tomcat server
 */
public class AppErrorController implements ErrorController {

  @RequestMapping("/error")
  public String index() {
    return "index.html";
  }

  @Override
  public String getErrorPath() {
    return "index.html";
  }
}
