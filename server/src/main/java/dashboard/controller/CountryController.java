package dashboard.controller;

import dashboard.dto.RestApiResponse;
import dashboard.model.Country;
import dashboard.service.CountryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(tags = "country")
public class CountryController {

  @Autowired
  private CountryService countryService;

  @GetMapping("/countries")
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 401, message = "Expired or invalid JWT token")
  })
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public RestApiResponse<List<Country>, Void> getCountryList() {
    return new RestApiResponse<>(countryService.getCountryList());
  }

}
