package dashboard.controller;

import javax.servlet.http.HttpServletRequest;

import dashboard.dto.*;
import dashboard.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import dashboard.model.User;
import dashboard.service.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/users")
@Api(tags = "users")
public class UserController {

  @Autowired
  private UserService userService;

  @PostMapping(value = "/login")
  @ResponseBody
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 422, message = "Invalid username/password supplied")
  })
  public RestApiResponse<TokenDTO, Void> login(@RequestBody LoginRequest loginRequest) {
    return new RestApiResponse<>(new TokenDTO(userService.login(loginRequest.getUsername(), loginRequest.getPassword())));
  }

  @PostMapping("/create")
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 422, message = "Username is already in use"),
    @ApiResponse(code = 401, message = "Expired or invalid JWT token")
  })
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public RestApiResponse<TokenDTO, Void> create(@ApiParam("To be created User") @RequestBody User user, HttpServletRequest req) {
    user.setRoles(new ArrayList<>(Arrays.asList(Role.ROLE_CLIENT)));
    User authenticatedUser = userService.whoAmI(req);
    if (authenticatedUser != null) {
      Long createdBy = authenticatedUser.getId();
      user.setCreatedBy(createdBy);
    }
    return new RestApiResponse<>(new TokenDTO(userService.create(user)));
  }

  @GetMapping("/related_users")
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 401, message = "Expired or invalid JWT token")
  })
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public RestApiResponse<List<User>, Void> getUsersRelatedToUser(HttpServletRequest req) {
    User authenticatedUser = userService.whoAmI(req);
    return new RestApiResponse<>(userService.findUsersByUser(authenticatedUser));
  }

  @DeleteMapping(value = "/{username}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 401, message = "Expired or invalid JWT token")
  })
  public RestApiResponse<String, Void> delete(@ApiParam("Username") @PathVariable String username) {
    userService.delete(username);
    return new RestApiResponse<>(username);
  }

  @PutMapping(value = "/update")
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 422, message = "Username is already in use"),
    @ApiResponse(code = 401, message = "Expired or invalid JWT token")
  })
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public RestApiResponse<User, Void> update(@ApiParam("To be created User") @RequestBody User user) {
    return new RestApiResponse<>(userService.updateUser(user));
  }

  @GetMapping(value = "/{username}")
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 401, message = "Expired or invalid JWT token")
  })
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public RestApiResponse<User, Void> search(@ApiParam("Username") @PathVariable String username) {
    return new RestApiResponse<>(userService.search(username));
  }

  @GetMapping("/refresh")
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 401, message = "Expired or invalid JWT token")
  })
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public RestApiResponse<TokenDTO, Void> refresh(HttpServletRequest req) {
    return new RestApiResponse<>(new TokenDTO(userService.refreshToken(req.getRemoteUser())));
  }

  @GetMapping(value = "/me")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 401, message = "Expired or invalid JWT token")
  })
  public  RestApiResponse<User, Void> whoAmI(HttpServletRequest req) {
    return new RestApiResponse<>(userService.whoAmI(req));
  }

}
