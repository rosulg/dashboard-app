package dashboard.exception;

import java.util.Map;

import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestAttributes;

@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GlobalExceptionHandlerController {

  @Bean
  public ErrorAttributes errorAttributes() {
    // Hide exception field in the return object
    return new DefaultErrorAttributes() {
      @Override
      public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {
      Map<String, Object> errorAttributes = super.getErrorAttributes(requestAttributes, includeStackTrace);
      errorAttributes.remove("exception");
      return errorAttributes;
      }
    };
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<ApiError> handleCustomException(CustomException ex) {
    return new ResponseEntity<>(createErrorObject(ex), ex.getHttpStatus());
  }

  @ExceptionHandler(AccessDeniedException.class)
  public ResponseEntity<ApiError> handleAccessDeniedException() {
    CustomException ex = new CustomException("Access denied", HttpStatus.FORBIDDEN);
    return new ResponseEntity<>(createErrorObject(ex), ex.getHttpStatus());
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ApiError> handleException() {
    CustomException ex = new CustomException("Something went wrong", HttpStatus.BAD_REQUEST);
    return new ResponseEntity<>(createErrorObject(ex), ex.getHttpStatus());
  }

  private ApiError createErrorObject(CustomException ex) {
    ApiError error = new ApiError();
    error.setError(ex.getHttpStatus().getReasonPhrase());
    error.setStatus(ex.getHttpStatus().value());
    error.setMessage(ex.getMessage());
    return error;
  }

}
