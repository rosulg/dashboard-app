package dashboard.dto;

public class RestApiResponse<DATA, METADATA> {
  private DATA data;
  private METADATA metadata;

  public RestApiResponse(DATA data, METADATA metadata) {
    this.data = data;
    this.metadata = metadata;
  }

  public RestApiResponse(DATA data) {
    this.data = data;
    this.metadata = null;
  }

  public DATA getData() {
    return data;
  }

  public METADATA getMetadata() {
    return metadata;
  }
}