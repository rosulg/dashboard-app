package dashboard.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
      .select()
      .apis(RequestHandlerSelectors.any())
      .paths(Predicates.not(PathSelectors.regex("/error")))
      .build()
      .apiInfo(metadata())
      .useDefaultResponseMessages(false)
      .securitySchemes(new ArrayList<>(Arrays.asList(new ApiKey("Bearer %token", "Authorization", "Header"))))
      .tags(new Tag("users", "Operations about users"))
      .tags(new Tag("error", "Handles errors, redirects everything to index.html"))
      .tags(new Tag("country", "Operations about countries"))
      .genericModelSubstitutes(Optional.class);

  }

  private ApiInfo metadata() {
    return new ApiInfoBuilder()//
      .title("User management API using JWT")
      .description(
            "Sample user management API that uses JWT. The default users are 'admin' and 'client' - both with password: 'password'. Send the JWT token with the authorization header and introduce it with the prefix \"Bearer \". This sample app is based of https://github.com/murraco/spring-boot-jwt")
      .version("1.0.0")//
      .license("MIT License").licenseUrl("http://opensource.org/licenses/MIT")
      .contact(new Contact(null, null, "robinsulg1@gmail.com"))
      .build();
  }

}
