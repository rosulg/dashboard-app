

-- Plain password = 'password'
INSERT INTO users (id, first_name, last_name, password, username) VALUES (1, 'Admin', 'Adminnus', '$2a$12$ATCaYSHvHxXJcmZsAq1KtuGuZFEmohUm1YDXt2cxSs2hYBdIyyKNS', 'admin');
INSERT INTO users (id, first_name, last_name, password, username) VALUES (2, 'Admin1', 'Adminnus1', '$2a$12$ATCaYSHvHxXJcmZsAq1KtuGuZFEmohUm1YDXt2cxSs2hYBdIyyKNS', 'admin1');
INSERT INTO users (id, first_name, last_name, password, username) VALUES (3, 'Admin2', 'Adminnus1', '$2a$12$ATCaYSHvHxXJcmZsAq1KtuGuZFEmohUm1YDXt2cxSs2hYBdIyyKNS', 'admin2');

-- Set roles for admin users
INSERT INTO user_roles (user_id, roles) VALUES (1, 0);
INSERT INTO user_roles (user_id, roles) VALUES (2, 0);
INSERT INTO user_roles (user_id, roles) VALUES (3, 0);

-- Insert related users (clients) to admin user
INSERT INTO users (id, first_name, last_name, password, username, created_by) VALUES (4, 'Client', 'Client', '$2a$12$ATCaYSHvHxXJcmZsAq1KtuGuZFEmohUm1YDXt2cxSs2hYBdIyyKNS', 'client', 1);
INSERT INTO users (id, first_name, last_name, password, username, created_by) VALUES (5, 'Client1', 'Client1', '$2a$12$ATCaYSHvHxXJcmZsAq1KtuGuZFEmohUm1YDXt2cxSs2hYBdIyyKNS', 'client1', 1);
INSERT INTO users (id, first_name, last_name, password, username, created_by) VALUES (6, 'Client2', 'Client2', '$2a$12$ATCaYSHvHxXJcmZsAq1KtuGuZFEmohUm1YDXt2cxSs2hYBdIyyKNS', 'client2', 1);

-- Set roles for client users
INSERT INTO user_roles (user_id, roles) VALUES (4, 1);
INSERT INTO user_roles (user_id, roles) VALUES (5, 1);

-- Add countries
INSERT INTO countries (name, code) VALUES ('Estonia', 'EE');
INSERT INTO countries (name, code) VALUES ('Latvia', 'LV');
INSERT INTO countries (name, code) VALUES ('Finland', 'FI');
INSERT INTO countries (name, code) VALUES ('United States of America', 'US');
INSERT INTO countries (name, code) VALUES ('United Kingdom', 'GB');

