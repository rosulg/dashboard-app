
-- Varchar and bigint lengths could be refactored to match the specific sizes declared by the app

CREATE TABLE countries (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  code varchar(2) DEFAULT NULL,
  PRIMARY KEY (id)
);


CREATE TABLE users (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  username varchar(255) UNIQUE NOT NULL,
  address varchar(255),
  country_code varchar(2), -- Correct country code value is not that important to add a foreign key. Foreign keys make the DB slower.
  created_by bigint(20),
  created_at timestamp default current_timestamp,
  -- Should also add updated_at
  PRIMARY KEY (id)
);


CREATE TABLE user_roles (
  user_id bigint(20) NOT NULL,
  roles bigint(20) NOT NULL,
  CONSTRAINT FK_user FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
);
