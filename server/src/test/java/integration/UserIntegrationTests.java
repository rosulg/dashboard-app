package integration;


import dashboard.JwtAuthServiceApp;
import dashboard.model.Role;
import dashboard.model.User;
import dashboard.repository.UserRepository;
import dashboard.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {JwtAuthServiceApp.class})
@Transactional
public class UserIntegrationTests {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserService userService;

  // TODO: Add test users here. Do not rely on sql-scripts/data.sql

  // The admin and client users are inserted in sql-scripts/data.sql.
  // The client user is related to the admin user.
  private Long adminUserId = 1L;
  private Long clientUserId = 6L;


  @Test
  public void shouldCreateAUserAndRetrieveJWT() {
    User user = new User();
    user.setFirstName("Ernest");
    user.setLastName("Hemingway");
    user.setUsername("hemingway");
    user.setPassword("password");
    user.setRoles(new ArrayList<>(Arrays.asList(Role.ROLE_CLIENT)));

    String token = userService.create(user);
    assertTrue(StringUtils.hasText(token));
  }

  @Test
  public void shouldLoginWithUser() {
    String loginToken = userService.login("admin", "password");
    assertTrue(StringUtils.hasText(loginToken));
  }

  @Test
  public void shouldUpdateAUser() {
    // The admin (id 1) user is inserted beforehand in sql-scripts/data.sql
    User user = userRepository.findOne(adminUserId);
    assertNotNull(user);
    user.setFirstName("Walt");
    user.setLastName("Whitman");
    User updatedUser = userService.updateUser(user);
    assertEquals(updatedUser.getFirstName(), user.getFirstName());
  }

  @Test
  public void shouldFindUserByUsername() {
    User user = userService.search("admin");
    assertNotNull(user);
    assertEquals(user.getUsername(), "admin");
  }

  @Test
  public void shouldFindUsersByUser() {
    User user = userRepository.findOne(adminUserId);
    List<User> userList = userService.findUsersByUser(user);
    assertTrue(userList.size() > 0);
  }

  @Test
  public void shouldDeleteAUser() {
    User user = userRepository.findOne(clientUserId);
    assertNotNull(user);
    userService.delete(user.getUsername());
    User deletedUSer = userRepository.findOne(clientUserId);
    assertNull(deletedUSer);
  }
}
