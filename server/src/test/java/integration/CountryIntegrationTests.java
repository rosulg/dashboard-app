package integration;

import dashboard.JwtAuthServiceApp;
import dashboard.model.Country;
import dashboard.service.CountryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {JwtAuthServiceApp.class})
@Transactional
public class CountryIntegrationTests {
  @Autowired
  private CountryService countryService;

  // TODO: Add countries here. Do not rely on sql-scripts/data.sql

  @Test
  public void shouldRetrieveListOfCountries() {
    List<Country> countryList = countryService.getCountryList();
    assertTrue(countryList.size() > 0);
  }
}
